from django.test import TestCase
from django.test import Client
from django.urls import reverse
from .forms import SignUpForm
from .views import signup
from .models import SignUpForm


class register(TestCase):
# Create your tests here.
	def test_register_url(self):
		response = Client().get('/register/')
		self.assertEqual(response.status_code,200)
	def test_register_url_not_exist(self):
		response = Client().get('hehe')
		self.assertEqual(response.status_code,404)
	def test_register_to_do_list_template(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response, 'App_RegisterPage_index.html')