from django import forms
from django.shortcuts import render, get_object_or_404
from django.forms.widgets import *

from .models import Testimoni

class TestiForm(forms.ModelForm):

    class Meta:
        model = Testimoni
        fields = ('testi_name', 'testi_message')
        labels = {
            'testi_name' : 'Nama untuk ditampilkan',
            'testi_message' : 'Testimoni Anda'
        }

        widgets = {
            'testi_name':forms.TextInput(attrs={'class':'form-control'}),
            'testi_message':forms.Textarea(attrs={'class':'form-control'}),
        }