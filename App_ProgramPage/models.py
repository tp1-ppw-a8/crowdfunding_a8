from django.db import models
# Create your models here.
class ProgramsModel(models.Model):
    title = models.CharField(max_length = 30)
    content = models.CharField(max_length = 300)
    organizer = models.CharField(max_length = 20)
    category = models.CharField(max_length = 20)
    amount = models.IntegerField(default = 0)
    foreign_news = models.ForeignKey('App_NewsPage.News', on_delete=models.CASCADE, default = 0)
    

