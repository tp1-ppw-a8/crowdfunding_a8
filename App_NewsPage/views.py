from django.shortcuts import render, get_object_or_404
from .models import News
from App_MakeDonationPage.models import Donate
from django.http import HttpResponseRedirect
# Create your views here.
response = {}
def newsPage(request):
    donate = Donate.objects.filter(donator=str(request.user.username))
    if(donate):
        total = 0
        for i in donate:
            total += i.amount
        response = {'news' : News.objects.all(), 'total' : total, 'donations' : donate}
    else:
        response = {'news' : News.objects.all()}
    
    return render(request, 'App_NewsPage_index.html', response)
    
