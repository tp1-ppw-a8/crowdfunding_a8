from django.apps import AppConfig


class AppNewspageConfig(AppConfig):
    name = 'App_NewsPage'
