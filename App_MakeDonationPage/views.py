from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Donate
from App_ProgramPage.models import ProgramsModel
from .forms import DonateForm
from django.utils import timezone
from django.shortcuts import redirect
from django.db.models import Sum
# Create your views here.
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

@login_required
def make_donation(request, id):
	program = ProgramsModel.objects.get(id = id)
	donations = Donate.objects.filter(foreign_program = program).order_by('-published_date')[:6]
	jumlah = Donate.objects.all().aggregate(sum = Sum('amount'))['sum']
	
	if request.method == "POST":
		form = DonateForm(request.POST)
		if form.is_valid():
			activity = form.save(commit=False)
			activity.donator = request.user.username
			activity.save()
			
			return redirect('/programs')
	else:
		form = DonateForm()
	return render(request, 'App_MakeDonationPage_index.html', {'form': form, 'table':donations, 'program':program, 'jumlah':jumlah})


