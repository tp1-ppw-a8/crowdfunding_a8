from django.db import models

# Create your models here.

class Testimoni(models.Model):
    testi_name = models.CharField(max_length = 100)
    testi_message = models.CharField(max_length = 1000)

    def submit(self):
        self.save()

    def __str__(self):
        return self.testi_name