from django.test import TestCase
from django.test import Client
from django.urls import reverse
from .forms import DonateForm
from .views import make_donation
from .models import Donate

class donation(TestCase):
# Create your tests here.
	def test_donation_url_not_exist(self):
		response = Client().get('hehe')
		self.assertEqual(response.status_code,404)
		
	def test_donation_url_is_exist(self):
		response = Client().get('//{0}/')
		self.assertEqual(response.status_code,200)

	def test_donation_is_using_template(self):
		response = Client().get('//{0}/')
		self.assertTemplateUsed(response, 'App_Index_index.html')
		
	def test_model_can_create_new_donation(self):
		new_donation = Donate.objects.create(amount = '9000')
		count_all_donation = Donate.objects.all().count()
		self.assertEqual(count_all_donation, 1)
		
	def test_amount_is_inserted(self):
		new_donation = Donate.objects.create(amount = '9000')
		self.assertIsNotNone(new_donation.amount)

