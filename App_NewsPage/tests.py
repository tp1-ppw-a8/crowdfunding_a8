from django.test import TestCase
from django.test import Client
from django.urls import reverse
from .models import News
from .views import newsPage

# Create your tests here.

class newsTest(TestCase) :
    
    def test_news_is_exist(self):
        response = Client().get(reverse('newsPage'))
        self.assertEqual(response.status_code,200)

    def test_news_using_template(self):
        response = Client().get(reverse('newsPage'))
        self.assertTemplateUsed(response, 'App_NewsPage_index.html')

    def test_model_can_create_new_news(self):
        new_news = News.objects.create(title = 'Gempa',news = ' aaaaaaaaaa ')
        count_all_news = News.objects.all().count()
        self.assertEqual(count_all_news, 1)