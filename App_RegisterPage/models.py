from django.core.validators import MaxLengthValidator
from django.db import models
from django.utils import timezone


class SignUpForm(models.Model):
    email = models.EmailField(max_length=254)
    date = models.DateTimeField(null=True)

