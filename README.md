# Crowd Funding A8<br>
This is the real project for 'Tugas Pemrograman1'
TP 1 PPW 2018<br>

Anggota Kelompok:
---------------
- Daniel Anderson Estefan		: 1706039641
- Gregorius Aprisunnea		    : 1706039710
- Muhammad Haikal Baihaqi		: 1706043411
- Yaumi Alfadha			        : 1706023031

Pipeline Status
--------------
[![pipeline status](https://gitlab.com/tp1-ppw-a8/crowdfunding_a8/badges/master/pipeline.svg)](https://gitlab.com/tp1-ppw-a8/crowdfunding_a8/commits/master)
<br>
Code Coverage
--------------
[![coverage report](https://gitlab.com/tp1-ppw-a8/crowdfunding_a8/badges/master/coverage.svg)](https://gitlab.com/tp1-ppw-a8/crowdfunding_a8/commits/master)

Heroku Link:
--------------
https://crowd-funding-a8.herokuapp.com/


