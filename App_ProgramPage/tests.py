from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest

from .views import index
from .models import ProgramsModel

class ProgramPageTest(TestCase):

    # check for templating and urls start
    def test_views_correct_template(self):
        response = Client().get(reverse('index_program'))
        self.assertTemplateUsed(response, 'App_ProgramPage_index.html')

    def test_programs_url_exist(self):
        response = Client().get(reverse('index_program'))
        self.assertEqual(response.status_code,200)

    def test_page_using_correct_views_func(self):
        found = resolve(reverse('index_program'))
        self.assertEqual(found.func, index)
    # check for templating and urls end
    
    # check for models start
    def test_models_can_create_new_program(self):
        new_program = ProgramsModel.objects.create(amount=1000, title="gempa palu", content="gempa yang menyebabkan korban jiwa", organizer="unicef", category="earthquake")
        # Retrieving all available activity
        counting_all_available_programs = ProgramsModel.objects.all().count()
        self.assertEqual(counting_all_available_programs, 1)

    def test_title_is_inserted(self):
        new_title = ProgramsModel.objects.create(title="Gempa Palu")
        self.assertIsNotNone(new_title.title)
    
    def test_content_is_inserted(self):
        new_content = ProgramsModel.objects.create(content="gempa yang menyebabkan korban jiwa")
        self.assertIsNotNone(new_content.content)

    def test_organizer_is_inserted(self):
        new_organizer = ProgramsModel.objects.create(organizer="unicef")
        self.assertIsNotNone(new_organizer.organizer)

    def test_category_is_inserted(self):
        new_category = ProgramsModel.objects.create(category="unicef")
        self.assertIsNotNone(new_category.category)

    def test_amount_is_inserted(self):
        new_amount = ProgramsModel.objects.create(amount=1000)
        self.assertIsNotNone(new_amount.amount)
    # check for models end



