from django.contrib import admin
from .models import SignUpForm

# Register your models here.
admin.site.register(SignUpForm)
