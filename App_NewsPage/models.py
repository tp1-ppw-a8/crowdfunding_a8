from django.db import models

# Create your models here.

class News(models.Model) :
    title = models.CharField(max_length = 200)
    image = models.URLField(max_length = 200, blank = True, null =True)
    news = models.TextField()

