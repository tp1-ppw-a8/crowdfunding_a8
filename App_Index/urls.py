from django.urls import path
from .views import index, add_testimoni

urlpatterns = [
    path('', index, name = 'index'),
    path('add_testimoni', add_testimoni, name = 'add_testimoni'),
]