from django import forms
from django.shortcuts import render, get_object_or_404
from django.forms.widgets import *

from .models import Donate

class DonateForm(forms.ModelForm):

	class Meta:
		model = Donate
		fields = ('amount',)
		labels = {
		'amount':'amount',
		}
		
		widgets = {
		'amount': forms.NumberInput(attrs={'class':'form-control'}),
		}
	
	def __init__(self, *args, **kwargs):
		super(DonateForm, self).__init__(*args, **kwargs)
		self.fields['amount'].widget.attrs['min'] = 1